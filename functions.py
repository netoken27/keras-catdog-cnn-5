# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 16:54:56 2020

@author: User
"""
import os
import numpy as np
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import load_img, img_to_array

def get_files(file_dir):
    cats = list()
    dogs = list()
    label_cats = list()
    label_dogs = list()
    for file in os.listdir(file_dir):
        name = file.split( sep='.' )
        if 'cat' in name[0]:
            img = load_img(file_dir + file)
            cats.append( img_to_array(img) )
            label_cats.append(0)
            
        else:
            if 'dog' in name[0]:
                img = load_img(file_dir + file)
                dogs.append( img_to_array(img) )
                label_dogs.append(1)
                
        image_list = np.hstack(( cats, dogs ))
        label_list = np.hstack(( label_cats, label_dogs))
        
    '''
    #float64 to int64.add images and label to temp, shuffle it.
    label_list = [ int(i) for i in label_list]
    temp = np.array( [image_list, label_list ])
    temp = temp.transpose()       
    np.random.shuffle( temp )
    
    image_list = list( map(str,temp[ :,0]))
    label_list = list( map(int,temp[ :,1]))
    
    train_x, test_x, train_y, test_y = train_test_split(image_list,
                                                        label_list,
                                                        test_size=0.2,
                                                        random_state=0)
    '''
    
    return cats, label_cats

if __name__=='__main__':
    cat, label_cats = get_files("F:/cat_dog/text/")
    