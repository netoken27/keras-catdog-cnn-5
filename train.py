# -*- coding: utf-8 -*-
"""
Created on Mon Feb 17 15:24:58 2020

@author: User
"""

import keras
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import functions
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential, Model
from keras.optimizers import RMSprop
from keras.layers import Activation, Dropout, Flatten, Dense, GlobalMaxPooling2D, Conv2D, MaxPooling2D
from keras.callbacks import CSVLogger
from livelossplot.keras import PlotLossesCallback
#import efficientnet.keras as efn

TRAINING_LOGS_FILE = "training_logs.csv"
MODEL_SUMMARY_FILE = "model_summary.txt"
MODEL_FILE = "catsDogs.h5"

DATA_DIR = "D:/cat_dog/all/"
train_x, test_x, train_y, test_y = functions.get_files(DATA_DIR)

IMAGE_SIZE = 200
IMAGE_WIDTH, IMAGE_HEIGHT = IMAGE_SIZE, IMAGE_SIZE
EPOCHS = 20
BATCH_SIZE = 32
TEST_SIZE = 30


input_shape = (IMAGE_WIDTH, IMAGE_HEIGHT, 3)

model = Sequential()
model.add(Conv2D(32, 3, 3, border_mode='same', input_shape = input_shape, activation = 'relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(64, 3, 3, border_mode='same', activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(128, 3, 3, border_mode='same', activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Conv2D(256, 3, 3, border_mode='same', activation='relu'))
model.add(MaxPooling2D(pool_size=(2,2)))

model.add(Flatten())
model.add(Dense(256, activation='relu'))
model.add(Dropout(0.5))

model.add(Dense(1))
model.add(Activation('sigmoid'))

model.compile(loss='binary_crossentropy',
              optimizer=RMSprop(lr=0.0001),
              metrics=['accuracy'])

with open(MODEL_SUMMARY_FILE, "w") as fw:
    model.summary(print_fn=lambda line: fw.write(line + "\n"))
    
training_data_generator = ImageDataGenerator(
    rescale=1/255.0,
    shear_range=0.1,
    zoom_range=0.1,
    horizontal_flip=True)
test_data_generator = ImageDataGenerator(rescale=1/255.0)

training_generator = training_data_generator.flow(np.array(train_x),
                                                  np.array(train_y),
                                                  batch_size=BATCH_SIZE)
testing_generator = test_data_generator.flow(np.array(test_x),
                                             np.array(test_y),
                                             batch_size=BATCH_SIZE)

model.fit_generator(
    training_generator,
    step_per_epoch=len(training_generator.filenames)/BATCH_SIZE,
    epoch=EPOCHS,
    validation_data=validation_generator,
    validation_steps=len(validation_generator.filenames)/BATCH_SIZE,
    callbacks=[PlotLossesCallback(), CSVLogger(TRAINING_LOGS_FILE,
                                               append=False,
                                               Separator=";")])

model.save_weights(MODEL_FILE)